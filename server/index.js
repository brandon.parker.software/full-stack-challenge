/*
    This is just a small sample to get you started. 
    Note that the docker setup will be looking for `index.js`,
    so it's best to use this file or the same file name.
 */
const express = require('express');
const cors = require('cors');
const { peopleHandler, planetHandler } = require('./handler.js')

const PORT = process.env.PORT || 4000;
const app = express();
app.use(cors())

app.get('/', (req, res) => res.send('Hello World!'));
app.get('/people/', peopleHandler);
app.get('/planets/', planetHandler);
app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
// npm run dev --prefix server    
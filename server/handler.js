
const swapi = require('swapi-node')
const sortBy = require('lodash/sortBy');

function getResidentNameFromUrl(url) {
    return new Promise(function (resolve, reject) {
      const parts = url.split('/')
      var id = parts[parts.length-2]
      swapi.getPerson({ id: id }).then(result => {
          resolve({ url: url, name: result.name })
        }).catch(error => {
            reject(error);
        });
    });
}


const peopleHandler = (req, res )=> {
    swapi.getPerson().then(result => {
      var sortField = req.query.sortBy
      if (sortField && ['name', 'height', 'mass'].includes(sortField)) {
        items = sortBy(items, (p) => (sortField == 'name' ? p[sortField] : +p[sortField]));
      }
      res.send(result)
    }).catch(error => {
      console.log(error);
    });
}

let promises = []
const planetHandler = (req, res )=> {
    swapi.getPlanets().then(result => {
        result.results.map(i => {
            i.residents.map(u => promises.push(getResidentNameFromUrl(u))) 
        })
      
        Promise.all(promises)
            .then(r => { 
                promises = []
                result.results.map(i => {
                    i.residents.map((e, i, a) => { // element, index, array
                        var person = r.find(({ url }) => url === e)
                        a[i] = person.name
                    });
                });
            }).then(_ => {
                res.send(result)
                })
                .catch(error => {
                    console.log(error);
                    }) 
                }).catch(error => {
            console.log(error);
    }); 
}

module.exports = { peopleHandler, planetHandler }

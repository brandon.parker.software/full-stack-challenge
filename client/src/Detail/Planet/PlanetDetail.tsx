import React from 'react';


type Props = {
  planet: Planet | null;
};

const PlanetDetail: React.FC<Props> = ({
     planet,
    }) => {

  return (
        <>
        <p>Name: {planet?.name}</p>
        <p>Number of Films: {planet?.films.length}</p>
        <p>Population: {planet?.population}</p>
        <p>Terrian: {planet?.terrain}</p>
        <p>Climate: {planet?.climate}</p>
        <p>Diameter: {planet?.diameter}</p>
        </>
  );
};

export default PlanetDetail;

import React from 'react';


type Props = {
  person: Person | null;
};

const PeopleDetail: React.FC<Props> = ({
     person,
    }) => {

  return (
        <>
        <p>Name: {person?.name}</p>
        <p>Number of Films: {person?.films.length}</p>
        <p>Birth Year: {person?.birth_year}</p>
        <p>Gender: {person?.gender}</p>
        <p>Height: {person?.height}</p>
        <p>Weight: {person?.mass}</p>
        </>
    
  );
};

export default PeopleDetail;

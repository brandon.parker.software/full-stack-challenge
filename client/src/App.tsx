import { useEffect, useState } from 'react';
import { Card, Row } from 'antd';
import  Panel from './Panel';
import './App.css'

function App() {
 
  const url = 'http://127.0.0.1:4000';

  const [planets, setPlanets] = useState<Array<Planet> | null>(null);
  const [persons, setPersons] = useState<Array<Person> | null>(null);

  const data = async() =>  {
    const planets = await fetch(`${url}/planets`).then((res) => res.json());
    const persons = await fetch(`${url}/people`).then((res) => res.json());
    setPlanets(planets.results) 
    setPersons(persons.results)
  }

  useEffect(() => { 
    data() 
  },[]);

  return (
    <div className="App">
      <Card title={'Star Warz'} style={{ width: '1200px' }}>
        <Row>
        <Panel persons={persons} planets={planets} />
        </Row>
      </Card>
    </div>
  )
}

export default App

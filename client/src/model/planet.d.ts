type Planet = {
    name: string;
    diameter: string;
    climate: string;
    gravity: string;
    terrain: string;
    population: string;
    residents: Array<string>;
    films: Array<string>;
  };
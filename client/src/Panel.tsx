import React, { useEffect, useState } from 'react';
import { Card, Col, Table, Input, Radio, RadioChangeEvent } from 'antd';
import PeopleDetail from './Detail/People/PeopleDetail'
import PlanetDetail from './Detail/Planet/PlanetDetail'

enum Category {
    People = 'people',
    Planets = 'planets'
}

type Props = {
    persons: Array<Person> | null;
    planets: Array<Planet> | null;
}

const Panel: React.FC<Props> = ({
    persons,
    planets
}) => {

    const [data, setData] = useState<Array<Person | Planet> | null>([]);
    const [search, setSearch] = useState<string>('');
    const [category, setCategory] = useState<Category>();
    const [selected, setSelected] = useState<Person | Planet | null>(null);

    useEffect(() => {
        const filtered = (category == Category.People) ? persons?.filter((items) => {
            return items.name.toLowerCase().includes(search.toLowerCase())
        }) || [] : planets?.filter((items) => {
            return items.name.toLowerCase().includes(search.toLowerCase())
        }) || []

        setData(filtered)
        console.log(filtered.length)
        
    },[search, category])

    const columns = [
        {
            title: "name",
            dataIndex: "name",
            key: "name",
            render: (name: string, item: Person | Planet) => (
                <span style={{ cursor: 'pointer'}} onClick={() => setSelected(item)}>
                { name }
                </span>
              ),
        },
        {
            title: "# of films",
            dataIndex: "films",
            key: "films",
            render: (items: Array<string>) => <span>{items.length}</span>,
        },
    ]

    const onSearchChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
        category && setSearch(e.target.value); setSelected(null);
    };

    const onCategoryChanged = (e: RadioChangeEvent) => {
        setCategory(e.target.value)
    }

    return (
        <>
        <span>Select a category&nbsp;&nbsp;  
            <Radio.Group value={category} onChange={(e) => onCategoryChanged(e)} >
                <Radio.Button key='people' value={Category.People}>people</Radio.Button>
                <Radio.Button key='planets' value={Category.Planets}>planets</Radio.Button>
            </Radio.Group>
        </span>
        <br></br>
        <br></br>
         <Input placeholder={`Search ${category} name`}
            onChange={(e) => onSearchChanged(e)}
            />
            <br></br>
        <Col span={10}>
            <Table
                style={{ width: '100%' }}
                dataSource={data ? data : []}
                columns={columns} 
            />
        </Col>
        <br></br>
        <Col span={10}>
            <Card title={'Info'}>
            { 
                category == Category.People && (
                <PeopleDetail person={(selected as Person)} />
                )
            }
            { 
            category == Category.Planets && (
                <PlanetDetail planet={(selected as Planet)} />
                )
            }
            </Card>
        </Col>
        </>
    );
};

export default Panel;
